# About
This project is created for you to get up to speed deploying to Medicloud quickly.
The project is based on a java-main-application using Maven with as few dependencies as possible.

Top level dependencies:

* jersey - "REST" library for java.
* jetty - lightweight http server
* logback - logging

The project builds an überjar/superjar/fatjar containing all the classes necessary to run, both from the command line and
in Medicloud.

Although this may not be your preferred stack, hopefully is shows as simply as possible ot to set up a similar project.
Our first take on this was a Clojure app, which works in a very similar way. However we decided Java was a more universal
for this example.

Licensed under the MIT-license. Do with this code as you please.

# Background
[Medicloud](https://users.medicloud.no) has choosen [IBM Bluemix](http://www.ibm.com/cloud-computing/bluemix/) as their platform for medical apps, therefore it is important
that user of Medicloud can create apps and deploy them easily to the platform.
Bluemix offers a variety of apps and services on the platform, so this application is an example of a mini application deployed.
Please give us feedback on possible improvements.

There is a lot of information in the official Bluemix [docs](https://console.ng.bluemix.net/docs).

# Prerequisite
[Git](https://git-scm.com/), [Maven](https://maven.apache.org/) and [Java](http://www.oracle.com/technetwork/java/javase/downloads/index-jsp-138363.html),
must be installed. You also need to retrieve a user-account for Medicloud, get in touch with your contact there to proceed.

Log into [Medicloud Bluemix](https://console.medicloud.eu-gb.bluemix.net) to see that you have access.

# Getting the code
Clone this repository to a local folder.

# Run the application locally
```
mvn clean install
java -jar target/medicloud-example-java-main.jar
```

Go and check that you get a sane response here:
[localhost/boss](http://localhost:8080/boss) and here [localhost](http://localhost:8080)


# Deploy to Medicloud
- Log into Medicloud and go to the [Bluemix dashboard](https://console.medicloud.eu-gb.bluemix.net/?direct=classic/#/resources).
- Download and install "Bluemix command line interface" and the "Cloud Foundry command line interface" from [here](https://clis.ng.bluemix.net/ui/home.html)
  and [here](https://github.com/cloudfoundry/cli/releases).
- Log into Bluemix from the command line (the org_name is shown in the top left corner of your dashboard, the space_name(s) are shown top left under "Create a Space"):

```
bluemix api https://api.medicloud.eu-gb.bluemix.net
bluemix login -u username -o org_name -s space_name
```

- Push the web-app to Medicloud:
`cf push medicloud-example-java-main -b java_buildpack -p target/medicloud-example-java-main.jar`
- In the log from the deploy there should be an url to the site, something like:
`urls: medicloud-example-java-main.medicloud.eu-gb.mybluemix.net`
- Open the web-app in your browser (e.g. http://medicloud-example-java-main.medicloud.eu-gb.mybluemix.net/boss)

# Updating the web-app

- Do your changes, make sure it runs locally.
- Push a new version to Medicloud (same as first deployment).
`cf push medicloud-example-java-main -b java_buildpack -p target/medicloud-example-java-main.jar`

# Any questions?

We hope this will kick start your development. Any if you find any errors in the repo, or want to 
contribute something, just open a pull request.

We are not, by any measure, experts in Bluemix or Medicloud, so we would generally refer you to the
[docs](https://console.ng.bluemix.net/docs) if you have questions.

But feel free to drop us an email if you have specific questions regarding the code or documentation
in the repo.

Regards [Stein Tore Tøsse](mailto:stein.tore@kodemaker.no) and [Alf Kristian Støyle](mailto:alf.kristian@kodemaker.no).
