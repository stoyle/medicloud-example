package no.medicloud;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            int port = getPort();
            Server server = new Server(port);

            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
            context.setContextPath("/");
            server.setHandler(context);

            ServletHolder jerseyServlet = context.addServlet(org.glassfish.jersey.servlet.ServletContainer.class, "/*");
            jerseyServlet.setInitOrder(1);
            jerseyServlet.setInitParameter("jersey.config.server.provider.packages","no.medicloud");

            ServletHolder staticServlet = context.addServlet(DefaultServlet.class,"/resources");
            staticServlet.setInitParameter("resourceBase","src/main/webapp");
            staticServlet.setInitParameter("pathInfoOnly","true");

            log.info("Starting app on port {}", port);
            server.start();
        } catch (Exception e) {
            log.error("Could not start", e);
            System.exit(-1);
        }
    }

    private static int getPort() {
        String port = System.getenv("PORT");
        if (port != null) {
            return Integer.parseInt(port);
        }
        return 8080;
    }
}
